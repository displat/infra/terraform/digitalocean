variable "name" {
  type        = string
  description = "Name used for the droplet. Must be valid for usage as a domain."
  default     = "displat"
}

variable "size" {
  type        = string
  description = "Size used for all nodes"
}

variable "image" {
  type        = string
  description = "File from Displat's HTTP image repository"
}

variable "region" {
  type        = string
  description = "Region used for all nodes"
}

variable "ssh_key" {
  type        = string
  description = "Path to the public SSH key used for connecting to the droplet as needed"
}

resource "digitalocean_droplet" "mono_aio" {
  name    = var.name
  image   = digitalocean_custom_image.aio_image.id
  region  = var.region
  size    = var.size
  ssh_key = [digitalocean_ssh_key.ssh_key.fingerprint]
}

resource "digitalocean_custom_image" "aio_image" {
  name    = "displat_aio"
  url     = "res.displat.net/images/${var.image}"
  regions = [var.image]
}

resource "digitalocean_ssh_key" "ssh_key" {
  name       = "Displat SSH Key"
  public_key = file(var.ssh_key)
}
